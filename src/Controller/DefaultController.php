<?php


namespace App\Controller;
session_start();
use Crew\Unsplash\HttpClient;
use Crew\Unsplash\Search;
use GuzzleHttp\Client;
use http\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Unsplash\OAuth2\Client\Provider\Unsplash;
use voku\helper\StopWords;

class DefaultController extends AbstractController
{

    /**
     * Nacitani fotek z API - hledam 24 fotek pro dany string, pak musim pro kazde ID posilat dalsi request
     * protoze GPS udaje nejsou v zakladnim vypisu
     * @Route("/refreshPictures")
     */
    public function refreshPictures(Request $request)
    {
        $client = new Client();
        $textToQuery = $_GET['textToQuery'];
        $url = "https://api.unsplash.com/search/photos?query=" . $textToQuery . "&per_page=24";
        $res = $client->request('GET', $url,
            ['headers' => [
                'Authorization' => 'Client-ID 0_5KwjYwn18ANZuNXW3BqPPbhPRHYofbRgYBnOO43_k',
                'Accept' => 'application/json'
            ],]);
        $images = [];
        $data = json_decode($res->getBody()->getContents(), true);
        foreach ($data["results"] as $image){
            $imageUrl = "https://api.unsplash.com/photos/" . $image["id"];
            $imageRes = $client->request('GET', $imageUrl,
                ['headers' => [
                    'Authorization' => 'Client-ID 0_5KwjYwn18ANZuNXW3BqPPbhPRHYofbRgYBnOO43_k',
                    'Accept' => 'application/json'
                ],]);
            $dataImage = json_decode($imageRes->getBody()->getContents(), true);
            $tmpImage = [];

            $tmpImage["likes"] = $dataImage["likes"];
            $tmpImage["description"] = $dataImage["description"] . " " . $dataImage["alt_description"];
            $tmpImage["url"] = $dataImage["urls"]['small'];

            if(!is_null($dataImage["location"]) && isset($dataImage["location"]["position"]) &&
                !is_null($dataImage["location"]["position"]["latitude"]) && !is_null($dataImage["location"]["position"]["longitude"])){
                $tmpImage["position"] = $dataImage["location"]["position"];
                $tmpImage["location"] = "Country: " . $dataImage["location"]["country"] . ", city: " . $dataImage["location"]["city"];
                $tmpImage["positionFound"] = true;
            } else {
                $tmpImage["positionFound"] = false;
                $tmpImage["location"] = "Údaje o lokaci nejsou k dispozici";
            }
            array_push($images, $tmpImage);
        }

        $_SESSION['images'] = $images;
        return $this->render('images.html.twig', ['images' => $images]);

    }

    //pocita vzdalenost mezi dvema body
    private function distanceInKmBetweenEarthCoordinates($lat1, $lon1, $lat2, $lon2) {
        $earthRadiusKm = 6371;

        $dLat = deg2rad ($lat2-$lat1);
        $dLon = deg2rad ($lon2-$lon1);

        $lat1 = deg2rad ($lat1);
        $lat2 = deg2rad ($lat2);

        $a = sin($dLat/2) * sin($dLat/2) + sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2);
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        //var_dump(round($earthRadiusKm * $c));
        return round($earthRadiusKm * $c);
    }

    //puvodni primitivni string distance funkce, ktera pocita od zacatku a nebere v potaz pocatecni rozdily v delce
    private function computeScorePrimitive($searchingWord, $descriptionWord){
        $lengthOfSearching = strlen($searchingWord);
        $lengthOfDescription = strlen($descriptionWord);
        $score = 0;

        if($lengthOfDescription > $lengthOfSearching){
            $score += ($lengthOfDescription - $lengthOfSearching);
        } else if($lengthOfDescription < $lengthOfSearching){
            $score += ($lengthOfSearching - $lengthOfDescription);
        }

        for($i = 0; $i < $lengthOfSearching; $i++){
            if($i >= $lengthOfDescription){
                break;
            }
            if($searchingWord[$i] != $descriptionWord[$i]){
                $score++;
            }
        }
        return $score;
    }

    //vypocet editacni vzdalenosti pomoci tabulky
    private function computeScore($searchingWord, $descriptionWord){
        $l1 = strlen($searchingWord);
        $l2 = strlen($descriptionWord);
        $dis = range(0, $l2);

        for ($x = 1; $x <= $l1; $x++)
        {
            $dis_new[0] = $x;
            for ($y = 1; $y <= $l2; $y++)
            {
                $c = ($searchingWord[$x - 1] == $descriptionWord[$y - 1]) ? 0 : 1;
                $dis_new[$y] = min($dis[$y] + 1, $dis_new[$y - 1] + 1, $dis[$y - 1] + $c);
            }
            $dis = $dis_new;
        }
        return $dis[$l2];
    }

    //z popisku odeberu stop slova a hledám pro každé slovo ze vstupu nej skore pomoci editacni vzdalenosti
    // vysledne skore je soucet nejnizsich hodnot pro kazde slovo
    private function editDistance($search, $description){
        $stopWords = new StopWords();
        $badwords = $stopWords->getStopWordsFromLanguage('en');
        $tmpArrayDescription = explode(" ", $description);
        $arraySearch = explode(" ", $search);
        $finalArrayDescription = [];
        foreach ($tmpArrayDescription as $word){
            if (!in_array($word, $badwords)) {
                array_push($finalArrayDescription, $word);
            }
        }
        $finalScore = 1;
        foreach ($arraySearch as $searchingWord){
            $scoreOfWord = 1000000;
            foreach ($finalArrayDescription as $descriptionWord){
                $score = $this->computeScore(strtoupper($searchingWord), strtoupper($descriptionWord));
                if($score < $scoreOfWord){
                    $scoreOfWord = $score;
                }
            }
            $finalScore += $scoreOfWord;
        }
        return $finalScore;
    }

    /**
     * Zmena poradi na zaklade hodnot z range pickeru
     * @Route("/modifyOrder")
     */
    public function modifyOrder(Request $request)
    {
        //pokud nejsou všechny GET parametry přítomné, vrátí se prázdné pole obrázků
        if(!(isset($_GET['likes']) && isset($_GET['location']) && isset($_GET['text']) && isset($_GET['description']) && isset($_GET['gps'])
            && isset($_SESSION['images']))) {
            return $this->render('order.html.twig', ['images' => []]);
        }
        $likes = floatval($_GET['likes']) / 100;
        $location = floatval($_GET['location']) / 100;
        $text = floatval($_GET['text']) / 100;

        $descriptionSearch = $_GET['description'];
        $descriptionSearch = trim($descriptionSearch, " ");
        $descriptionSearch = preg_replace('!\s+!', ' ', $descriptionSearch);

        $gps = $_GET['gps'];
        $images = $_SESSION['images'];
        $maxLikes = 0;
        $minDistance = 1000000000;
        $minScore = 10000000000;
        $newImages = [];

        //v procházím obrázky a počítám skóre
        foreach ($images as $image){
            //procházím obrázky a počítám skóre
            if($image["positionFound"] == false){
                $image["distance"] = 1000000000;
            } else {
                $tmpDistance = floatval($this->distanceInKmBetweenEarthCoordinates(floatval($image["position"]["latitude"]),
                    floatval($image["position"]["longitude"]), floatval($gps[1]), floatval($gps[0])));
                if($tmpDistance == 0){
                    $tmpDistance = 1;
                }
                if($tmpDistance < $minDistance){
                    $minDistance = $tmpDistance;
                }
                $image["distance"] = $tmpDistance;
            }

            $tmpTextScore = floatval($this->editDistance($descriptionSearch, $image["description"]));
            if($tmpTextScore < $minScore){
                $minScore = $tmpTextScore;
            }
            $image["textScore"] = $tmpTextScore;

            if(floatval($image["likes"]) > $maxLikes){
                $maxLikes = floatval($image["likes"]);
            }
            array_push($newImages, $image);
        }
        $_SESSION['images'] = $newImages;

        //seradim obrazky na zaklade procent z range pickeru, které vynasobim z vypocitanym skore
        usort($newImages, function ($a, $b) use ($likes, $location, $text, $maxLikes, $minDistance, $minScore) {
            $scoreA = ($location * ($minDistance / floatval($a["distance"]))) + ($likes * (floatval($a["likes"]) / $maxLikes)) +
                ($text * ($minScore / floatval($a["textScore"])));

            $scoreB = ($location * ($minDistance / floatval($b["distance"]))) + ($likes * (floatval($b["likes"]) / $maxLikes)) +
                ($text * ($minScore / floatval($b["textScore"])));
            if($scoreA > $scoreB){
                return -1;
            } elseif ($scoreA < $scoreB){
                return 1;
            } else {
                return 0;
            }
        });
        return $this->render('order.html.twig', ['images' => $newImages]);
    }

    public function index(Request $request)
    {
        if(!isset($_SESSION['images']))
            $_SESSION['images'] = [];
        $images = $_SESSION['images'];
        return $this->render('base.html.twig', ['images' => $images]);
    }
}